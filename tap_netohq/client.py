"""REST client handling, including netohqStream base class."""

import requests
import json
from typing import Any, Optional
from backports.cached_property import cached_property

from singer_sdk.exceptions import FatalAPIError
from singer_sdk.streams import RESTStream

class netohqStream(RESTStream):
    """netohq stream class."""

    rest_method = "POST"
    action = None
    path = None
    paginated = True

    records_jsonpath = "$[*]" 

    @property
    def url_base(self):
        url =f"{self.config['url']}/do/WS/NetoAPI"
        return url

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        headers["NETOAPI_USERNAME"] = self.config["username"]
        headers["NETOAPI_KEY"] = self.config["api_key"]
        headers["NETOAPI_ACTION"] = self.action
        headers["Accept"] = 'application/json'
        headers["Content-Type"] = 'application/json'
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:

        if not self.paginated:
            return None

        if previous_token is None:
            return 1
        
        if len(list(self.parse_response(response))) == 0:
            return None

        return previous_token + 1
        
    def validate_response(self, response: requests.Response) -> None:
        super().validate_response(response)

        resp = response.json()

        if resp.get('success') == False:
            msg = ''
            for error in resp.get('errors'):
                msg += f" Client Error: {error.get('code')}\n message: {error.get('message')} \n"
            raise FatalAPIError(msg)
        
        if resp.get('Ack') == "Error":
            error = resp.get('Messages')
            if isinstance(error, str):
                msg = f" Client Error: {error} \n"
            else:
                msg = f" Client Error: {json.dumps(error)} \n"
            raise FatalAPIError(msg)

    @cached_property
    def selected_properties(self):
        orderline_properties = ["UnitPrice", "CostPrice", "WarehouseName", "ShippingMethod", "ShippingTracking", 
                "ProductDiscount", "WarehouseID", "BackorderQuantity", "WarehouseReference", 
                "ProductName", "Quantity", "PercentDiscount", "SKU", "OrderLineID", "PickQuantity"]
        selected_properties = []
        for key, value in self.metadata.items():
            if isinstance(key, tuple) and len(key) == 2 and value.selected:
                selected_properties.append(key[-1])
                if key[-1] == "OrderLine":
                    for prop in orderline_properties:
                        selected_properties.append(key[-1]+ "." + prop)
        return selected_properties

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:

        if next_page_token is None:
            params_page = 0
        else:
            params_page = next_page_token

        filter_dict = { "Approved": True,
                        "Page": params_page,
                        "Limit": 100 }

        if self.extra_filters:
            filter_dict.update(self.extra_filters)

        if self.replication_key:
            filter_date = self.get_starting_replication_key_value(context)
            filter_date = filter_date.replace('Z','').replace('T',' ')
            filter_dict["DateUpdatedFrom"] = filter_date
            
        filter_dict["OutputSelector"] = self.selected_properties
        if self.name == "orders":
            filter_dict['OutputSelector'].extend(['ShipAddress', 'BillAddress'])
        request_data = {
            "Filter": filter_dict
        }
        return request_data