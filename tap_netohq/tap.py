"""netohq tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  
from tap_netohq.streams import (
    ProductStream,
    OrderStream,
    VocherStream,
    CurrencyStream,
    SupplierStream,
    WarehouseStream
)

STREAM_TYPES = [
    ProductStream,
    OrderStream,
    VocherStream,
    CurrencyStream,
    SupplierStream,
    WarehouseStream

]


class Tapnetohq(Tap):
    """netohq tap class."""
    name = "tap-netohq"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "username",
            th.StringType,
            required=True
        ),
        th.Property(
            "api_key",
            th.StringType,
            required=True
        ),
        th.Property(
            "url",
            th.StringType,
            required=True
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync"
        ),
    ).to_dict()
    

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    Tapnetohq.cli()
