"""Stream type classes for tap-netohq."""

from typing import Any, Optional

from singer_sdk import typing as th

from tap_netohq.client import netohqStream


class OrderStream(netohqStream):
    """Define custom stream."""

    name = "orders"
    primary_keys = ["OrderID"]
    replication_key = "DateUpdated"
    records_jsonpath = "$.Order[*]"
    action = "GetOrder"
    extra_filters = {
        "OrderStatus": ["New", "Pick", "Pack"],
        "UpdateResults": {"ExportStatus": "Exported"},
        }

    schema = th.PropertiesList(
        th.Property("DateInvoiced", th.DateTimeType),
        th.Property("DatePaid", th.DateTimeType),
        th.Property("DateUpdated", th.DateTimeType),
        th.Property("OrderType", th.StringType),
        th.Property("Username", th.StringType),
        th.Property("DeliveryInstruction", th.StringType),
        th.Property("Email", th.StringType),
        th.Property("ShippingOption", th.StringType),
        th.Property("ShippingSignature", th.StringType),
        th.Property("BillLastName", th.StringType),
        th.Property("ShipStreetLine1", th.StringType),
        th.Property("ShipStreetLine2", th.StringType),
        th.Property("ShipState", th.StringType),
        th.Property("ShipPhone", th.StringType),
        th.Property("ShipCity", th.StringType),
        th.Property("SalesChannel", th.StringType),
        th.Property("OrderPayment", th.ArrayType(th.CustomType({"type":["object","string"]})) ),
        th.Property("ShippingDiscount", th.StringType),
        th.Property("DatePlaced", th.DateTimeType),
        th.Property("OrderStatus", th.StringType),
        th.Property("DateRequired", th.DateTimeType),
        th.Property("GrandTotal", th.StringType),
        th.Property("BillStreetLine1", th.StringType),
        th.Property("BillState", th.StringType),
        th.Property("BillCountry", th.StringType),
        th.Property("BillPostCode", th.StringType),
        th.Property("OrderID", th.StringType),
        th.Property("ShipFirstName", th.StringType),
        th.Property("ShipLastName", th.StringType),
        th.Property("CustomerRef2", th.StringType),
        th.Property(
            "OrderLine",
            th.ArrayType(
                th.ObjectType(
                    th.Property("UnitPrice", th.StringType),
                    th.Property("CostPrice", th.StringType),
                    th.Property("WarehouseName", th.StringType),
                    th.Property("ShippingMethod", th.StringType),
                    th.Property("ShippingTracking", th.StringType),
                    th.Property("ProductDiscount", th.StringType),
                    th.Property("WarehouseID", th.StringType),
                    th.Property("BackorderQuantity", th.StringType),
                    th.Property("WarehouseReference", th.StringType),
                    th.Property("ProductName", th.StringType),
                    th.Property("Quantity", th.StringType),
                    th.Property("PercentDiscount", th.StringType),
                    th.Property("SKU", th.StringType),
                    th.Property("OrderLineID", th.StringType),
                    th.Property("PickQuantity", th.StringType),
                )
            ),
        ),
        th.Property("BillFirstName", th.StringType),
        th.Property("BillCity", th.StringType),
        th.Property("CustomerRef4", th.StringType),
        th.Property("ShipCountry", th.StringType),
        th.Property("ShippingTotal", th.StringType),
        th.Property("CustomerRef3", th.StringType),
        th.Property("CustomerRef1", th.StringType),
        th.Property("ShipPostCode", th.StringType),
    ).to_dict()


class WarehouseStream(netohqStream):
    """Define custom stream."""

    name = "warehouses"
    primary_keys = ["ID"]
    replication_key = None
    records_jsonpath = "$.Warehouse[*]"
    action = "GetWarehouse"
    extra_filters = None

    schema = th.PropertiesList(
        th.Property("ID", th.StringType),
        th.Property("WarehousePostcode", th.StringType),
        th.Property("IsActive", th.StringType),
        th.Property("WarehouseContact", th.StringType),
        th.Property("WarehouseID", th.StringType),
        th.Property("ShowQuantity", th.StringType),
        th.Property("WarehouseStreet1", th.StringType),
        th.Property("WarehouseName", th.StringType),
        th.Property("WarehouseState", th.StringType),
        th.Property("WarehouseCountry", th.StringType),
        th.Property("WarehouseCity", th.StringType),
        th.Property("WarehouseStreet2", th.StringType),
        th.Property("WarehousePhone", th.StringType),
        th.Property("WarehouseNotes", th.StringType),
        th.Property("IsPrimary", th.StringType),
        th.Property("WarehouseReference", th.StringType),
    ).to_dict()


class VocherStream(netohqStream):
    """Define custom stream."""

    name = "vouchers"
    primary_keys = ["VoucherID"]
    replication_key = "DateUpdated"
    records_jsonpath = "$.Voucher[*]"
    action = "GetVoucher"
    extra_filters = None
    paginated = False

    schema = th.PropertiesList(
        th.Property("VoucherID", th.StringType),
        th.Property("VoucherProgramID", th.StringType),
        th.Property("VoucherTitle", th.StringType),
        th.Property("VoucherCode", th.StringType),
        th.Property("SecretKey", th.StringType),
        th.Property("IsRedeemed", th.StringType),
        th.Property("Owner", th.StringType),
        th.Property("Email", th.StringType),
        th.Property("CreatedFromOrderID", th.StringType),
        th.Property("DateAdded", th.DateTimeType),
        th.Property("DateUpdated", th.DateTimeType),
        th.Property("Balance", th.StringType),
        th.Property("IsSingleUse", th.StringType),
        th.Property("VoucherProgram", th.StringType),
        th.Property("ProgramType", th.StringType),
        th.Property(
            "ListOfCreditHistory",
            th.ObjectType(
                th.Property(
                    "CreditHistory",
                    th.ArrayType(
                        th.ObjectType(
                            th.Property("OrderID", th.StringType),
                            th.Property("Status", th.StringType),
                            th.Property("AmountCredited", th.StringType),
                            th.Property("TotalAmountUsed", th.StringType),
                            th.Property("Balance", th.StringType),
                            th.Property("DateAdded", th.DateTimeType),
                            th.Property("ExpiryDate", th.DateTimeType),
                        )
                    ),
                )
            ),
        ),
        th.Property(
            "ListOfUsageHistory",
            th.ObjectType(
                th.Property(
                    "UsageHistory",
                    th.ArrayType(
                        th.ObjectType(
                            th.Property("OrderID", th.StringType),
                            th.Property("PaymentID", th.StringType),
                            th.Property("Amount", th.StringType),
                            th.Property("DateUsed", th.DateTimeType),
                        )
                    ),
                )
            ),
        ),
    ).to_dict()


class CurrencyStream(netohqStream):
    """Define custom stream."""

    name = "currencies"
    primary_keys = ["DEFAULTCURRENCY"]
    replication_key = None
    records_jsonpath = "$.CurrencySettings[*]"
    action = "GetCurrencySettings"
    extra_filters = None
    paginated = False


    schema = th.PropertiesList(
        th.Property("DEFAULTCURRENCY", th.StringType),
        th.Property("DEFAULTCOUNTRY", th.StringType),
        th.Property("GST_AMT", th.StringType),
    ).to_dict()

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:
        return {}


class SupplierStream(netohqStream):
    """Define custom stream."""

    name = "suppliers"
    path = None
    primary_keys = ["ID"]
    replication_key = None
    records_jsonpath = "$.Supplier[*]"
    action = "GetSupplier"
    extra_filters = None


    schema = th.PropertiesList(
        th.Property("ID", th.StringType),
        th.Property("SupplierID", th.StringType),
        th.Property("SupplierReference", th.StringType),
        th.Property("LeadTime1", th.StringType),
        th.Property("LeadTime2", th.StringType),
        th.Property("SupplierCompany", th.StringType),
        th.Property("SupplierStreet1", th.StringType),
        th.Property("SupplierStreet2", th.StringType),
        th.Property("SupplierCity", th.StringType),
        th.Property("SupplierState", th.StringType),
        th.Property("SupplierPostcode", th.StringType),
        th.Property("SupplierCountry", th.StringType),
        th.Property("SupplierEmail", th.StringType),
        th.Property("SupplierPhone", th.CustomType({"type":["array","object","string"]})),
        th.Property("SupplierFax", th.StringType),
        th.Property("SupplierURL", th.StringType),
        th.Property("ExportTemplate", th.StringType),
        th.Property("SupplierCurrencyCode", th.StringType),
        th.Property("AccountCode", th.StringType),
        th.Property("FactoryStreet1", th.StringType),
        th.Property("FactoryStreet2", th.StringType),
        th.Property("FactoryCity", th.StringType),
        th.Property("FactoryState", th.StringType),
        th.Property("FactoryPostcode", th.StringType),
        th.Property("FactoryCountry", th.StringType),
        th.Property("SupplierNotes", th.StringType),
    ).to_dict()

class ProductStream(netohqStream):
    """Define custom stream."""

    name = "products"
    path = None
    primary_keys = ["ID"]
    replication_key = "DateUpdated"
    records_jsonpath = "$.Item[*]"
    action = "GetItem"
    extra_filters = None


    schema = th.PropertiesList(
        th.Property("ShippingHeight", th.StringType),
        th.Property("Features", th.StringType),
        th.Property("TaxCategory", th.StringType),
        th.Property("EditableKitBundle", th.StringType),
        th.Property("IsActive", th.StringType),
        th.Property("eBayDescription", th.StringType),
        th.Property("Misc24", th.StringType),
        th.Property("IsBought", th.StringType),
        th.Property("UPC3", th.StringType),
        th.Property("ShippingHeight", th.StringType),
        th.Property("IsGiftVoucher", th.StringType),
        th.Property("PromotionExpiryDateUTC", th.StringType),
        th.Property("Misc03", th.StringType),
        th.Property("SubType", th.StringType),
        th.Property("ReferenceNumber", th.StringType),
        th.Property("Misc32", th.StringType),
        th.Property("ProductURL", th.StringType),
        th.Property("Misc16", th.StringType),
        th.Property("ImageURL", th.StringType),
        th.Property("Group", th.StringType),
        th.Property("Misc30", th.StringType),
        th.Property("ShortDescription", th.StringType),
        th.Property("PreOrderQuantity", th.StringType),
        th.Property("TaxFreeItem", th.StringType),
        th.Property("SellUnitQuantity", th.StringType),
        th.Property("Brand", th.StringType),
        th.Property("BaseUnitPerQuantity", th.StringType),
        th.Property(
            "PriceGroups",th.CustomType({"type":["array","object","string"]})
        ),
        th.Property(
            "ItemSpecifics", th.ArrayType(th.CustomType({"type": ["object", "string"]}))
        ),
        th.Property("DateAdded", th.StringType),
        th.Property("Subtitle", th.StringType),
        th.Property("TermsAndConditions", th.StringType),
        th.Property("PickZone", th.StringType),
        th.Property("AvailableSellQuantity", th.StringType),
        th.Property("Misc13", th.StringType),
        th.Property("RestrictedToUserGroup", th.StringType),
        th.Property("BaseUnitOfMeasure", th.StringType),
        th.Property("Misc41", th.StringType),
        th.Property("ShippingLength", th.StringType),
        th.Property("Misc05", th.StringType),
        th.Property("Misc49", th.StringType),
        th.Property(
            "Categories", th.ArrayType(th.CustomType({"type": ["object", "string"]}))
        ),
        th.Property("NumbersOfLabelsToPrint", th.StringType),
        th.Property("WarehouseQuantity", th.CustomType({"type": ["array","object", "string"]})),
        th.Property("Misc38", th.StringType),
        th.Property("AccountingCode", th.StringType),
        th.Property("Misc42", th.StringType),
        th.Property("AvailabilityDescription", th.StringType),
        th.Property("DateAddedLocal", th.StringType),
        th.Property(
            "SalesChannels",
            th.ObjectType(
                th.Property(
                    "SalesChannel", 
                        th.ObjectType(
                            th.Property("SalesChannelName", th.StringType),
                            th.Property("SalesChannelID", th.StringType),
                            th.Property("IsApproved", th.StringType),
                    )
                )
            ),
        ),
        th.Property("SEOPageTitle", th.StringType),
        th.Property("Misc35", th.StringType),
        th.Property("DateUpdatedLocal", th.StringType),
        th.Property("AutomaticURL", th.StringType),
        th.Property("UPC1", th.StringType),
        th.Property("ExpenseAccount", th.StringType),
        th.Property("SortOrder2", th.StringType),
        th.Property("Misc37", th.StringType),
        th.Property("SplitForWarehousePicking", th.StringType),
        th.Property("Misc02", th.StringType),
        th.Property("AssetAccount", th.StringType),
        th.Property("UnitOfMeasure", th.StringType),
        th.Property("Job", th.StringType),
        th.Property("PickPriority", th.StringType),
        th.Property("Misc52", th.StringType),
        th.Property("Misc28", th.StringType),
        th.Property("RequiresPackaging", th.StringType),
        th.Property("SerialTracking", th.StringType),
        th.Property("ItemLength", th.StringType),
        th.Property("CommittedQuantity", th.StringType),
        th.Property("Misc44", th.StringType),
        th.Property("Virtual", th.StringType),
        th.Property("ID", th.StringType),
        th.Property("Misc17", th.StringType),
        th.Property("Misc07", th.StringType),
        th.Property("SortOrder1", th.StringType),
        th.Property("PromotionStartDateUTC", th.StringType),
        th.Property("Misc11", th.StringType),
        th.Property("SEOPageHeading", th.StringType),
        th.Property("SEOMetaDescription", th.StringType),
        th.Property("PromotionPrice", th.StringType),
        th.Property("PromotionExpiryDate", th.StringType),
        th.Property("Misc25", th.StringType),
        th.Property("Misc23", th.StringType),
        th.Property("PromotionStartDateLocal", th.StringType),
        th.Property("Misc50", th.StringType),
        th.Property("SKU", th.StringType),
        th.Property("Misc29", th.StringType),
        th.Property("BrochureURL", th.StringType),
        th.Property("Misc22", th.StringType),
        th.Property("Model", th.StringType),
        th.Property("Misc43", th.StringType),
        th.Property("SEOCanonicalURL", th.StringType),
        th.Property("DateArrival", th.StringType),
        th.Property("Misc04", th.StringType),
        th.Property("ShippingWidth", th.StringType),
        th.Property("Misc48", th.StringType),
        th.Property("SearchKeywords", th.StringType),
        th.Property("Misc15", th.StringType),
        th.Property("Misc09", th.StringType),
        th.Property("WhenToRepeatOnStandingOrders", th.StringType),
        th.Property(
            "Images",
            th.ArrayType(
                th.ObjectType(
                    th.Property("URL", th.StringType),
                    th.Property("Timestamp", th.StringType),
                    th.Property("ThumbURL", th.StringType),
                    th.Property("MediumThumbURL", th.StringType),
                    th.Property("Name", th.StringType),
                )
            ),
        ),
        th.Property("Misc39", th.StringType),
        th.Property("CostOfSalesAccount", th.StringType),
        th.Property("PrimarySupplier", th.StringType),
        th.Property("Misc10", th.StringType),
        th.Property("Misc34", th.StringType),
        th.Property("Misc19", th.StringType),
        th.Property("Misc33", th.StringType),
        th.Property(
            "WarehouseLocations",
            th.ArrayType(
                    th.Property("WarehouseLocation", th.ObjectType(
                        th.Property("LocationID",th.StringType),
                        th.Property("Priority",th.StringType),
                        th.Property("Type",th.StringType),
                        th.Property("WarehouseID",th.StringType),
                    )
                )
            ),
        ),
        th.Property("ItemHeight", th.StringType),
        th.Property("AuGstExempt", th.StringType),
        th.Property("Misc46", th.StringType),
        th.Property("FreeGifts", th.ArrayType(th.StringType)),
        th.Property("Misc20", th.StringType),
        th.Property("eBayProductIDs",
            th.Property("eBayProductID", 
                th.CustomType({"type":["object","array","string"]})
            )
        ),
        th.Property("CostPrice", th.StringType),
        th.Property("Misc40", th.StringType),
        th.Property("Warranty", th.StringType),
        th.Property("PromotionStartDate", th.StringType),
        th.Property("DateAddedUTC", th.StringType),
        th.Property("Misc31", th.StringType),
        th.Property("BarcodeHeight", th.StringType),
        th.Property("DefaultPrice", th.StringType),
        th.Property("BuyUnitQuantity", th.StringType),
        th.Property("Misc36", th.StringType),
        th.Property("DateUpdated", th.DateTimeType),
        th.Property("Misc14", th.StringType),
        th.Property("Name", th.StringType),
        th.Property("Specifications", th.StringType),
        th.Property("Misc01", th.StringType),
        th.Property("DateUpdatedUTC", th.StringType),
        th.Property("IsSold", th.StringType),
        th.Property("ModelNumber", th.StringType),
        th.Property("QuantityPerScan", th.StringType),
        th.Property("UPC2", th.StringType),
        th.Property("Misc45", th.StringType),
        th.Property("Format", th.StringType),
        th.Property("Approved", th.StringType),
        th.Property("IsAsset", th.StringType),
        th.Property("CrossSellProducts", th.StringType),
        th.Property("Type", th.StringType),
        th.Property("teIsNetoUtilityst", th.StringType),
        th.Property("ItemURL", th.StringType),
        th.Property("DisplayTemplate", th.StringType),
        th.Property("MonthlySpendRequirement", th.StringType),
        th.Property("ArtistOrAuthor", th.StringType),
        th.Property("Misc51", th.StringType),
        th.Property("IncomeAccount", th.StringType),
        th.Property("Misc06", th.StringType),
        th.Property("CubicWeight", th.StringType),
        th.Property("Misc47", th.StringType),
        th.Property("DateArrivalUTC", th.StringType),
        th.Property("HandlingTime", th.StringType),
        th.Property("ShippingWeight", th.StringType),
        th.Property("PromotionExpiryDateLocal", th.StringType),
        th.Property("PurchaseTaxCode", th.StringType),
        th.Property("IsVariant", th.StringType),
        th.Property("Misc27", th.StringType),
        th.Property("DateCreatedUTC", th.StringType),
        th.Property("RelatedContents", th.CustomType({"type":["object","array","string"]})),
        th.Property("DateCreatedLocal", th.StringType),
        th.Property("NzGstExempt", th.StringType),
        th.Property("InternalNotes", th.StringType),
        th.Property("Misc18", th.StringType),
        th.Property("SEOMetaKeywords", th.StringType),
        th.Property("TaxInclusive", th.StringType),
        th.Property("RRP", th.StringType),
        th.Property("UpsellProducts", th.CustomType({"type":["array","object","string"]})),
        th.Property("ParentSKU", th.StringType),
        th.Property("ItemWidth", th.StringType),
        th.Property("Misc12", th.StringType),
        th.Property("Misc26", th.StringType),
        th.Property("Misc08", th.StringType),
        th.Property("ShippingCategory", th.StringType),
        th.Property("IsInventoried", th.StringType),
        th.Property("InventoryID", th.StringType),
        th.Property("UPC", th.StringType),
        th.Property(
            "KitComponents",
            th.CustomType({"type":["array","object","string"]})
        ),
        th.Property("Description", th.StringType),
    ).to_dict()
